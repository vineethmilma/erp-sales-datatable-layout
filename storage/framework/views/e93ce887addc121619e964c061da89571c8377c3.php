<?php $__env->startSection('thispageleftsidebar'); ?>

<!-- left navigation bar -->
      <nav class="navbar navbar-inverse erpsidebar schemecolor3bg navbar-fixed-left" style="top:8%; z-index:10;border:none;" >
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header ">
      <button type="button" class="pull-left navbar-toggle collapsed schemecolor1bg" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <!-- <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span> -->
        <span><i class="fa fa-chevron-circle-down schemecolor2fg" aria-hidden="true"></i>
</span>
      </button>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
      <ul class="nav navbar-nav text-center">
        <li class="active"><a href="#">MANAGE ORGANISATION <span class="sr-only">(current)</span></a></li>
          <li ><a href="#" class="sidebarlink schemecolor1fg">ADDRESS BOOK</a></li>
      <li class="accordion">ADDRESS MASTERS<span><i style="margin-left:5px;" class="fa fa-sort-desc fa-1x"  aria-hidden="true"></i></span></li>
  <div class="panel text-center schemecolor2bg schemecolor1fg">
    <li style="padding:10px;">MANAGE STATES</li>
    <li style="padding:10px;">MANAGE DISTRICTS</li>
    <li style="padding:10px;">MANAGE TALUKS</li>
    <li style="padding:10px;">MANAGE BLOCKS</li>
    <li style="padding:10px;">MANAGE PANCHAYATHS</li>
    <li style="padding:10px;">MANAGE WARDS</li>
  </div>




      </ul>


    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<!-- end of left navigation bar -->

<?php $__env->stopSection(); ?>

<?php $__env->startSection('thispagehtml'); ?>
<br>
<br>
<br>
<br>
<br>
<table id="magictable" class="table table-striped table-bordered table-hover display">
    <thead>
        <tr>
            <th>id</th>
            <th>Name</th>
            <th>Price</th>
            <th>Stock</th>
            <th>Discount</th>
        </tr>
    </thead>
    <tbody>
      <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $eachproduct): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <tr>
        <td><?php echo e($eachproduct->id); ?></td>
        <td><?php echo e($eachproduct->name); ?></td>
        <td><?php echo e($eachproduct->price); ?></td>
        <td><?php echo e($eachproduct->stock); ?></td>
        <td><?php echo e($eachproduct->discount); ?></td>
      </tr>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </tbody>
</table>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('thispagejavascript_jquery'); ?>
<script>
$(document).ready(function(){
// console.log('i am here');
var mytable = $('#magictable').DataTable({
    paging:false,
    scrollY: '50vh',
    "columnDefs": [
   { "searchable": false, "targets": 0 },
   {"orderable":false, "targets":1},
   { "targets":4,render:$.fn.dataTable.render.percentBar()}
 ],
    "scrollCollapse": true,
     autoFill: true,
     select:true,
     responsive:true,
     "processing":true,
    "autoWidth":true,
         dom: 'B<"clear">l<"row"<"col-sm-offset-3 col-lg-4"f>>rtip',
    buttons: ['copy',{ extend: 'print', text: 'Export as pdf', className:"btn schemecolor2bg schemecolor1fg" },'excel'],
      stateSave: true

  });







});
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('erp-layout.sitelayout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>